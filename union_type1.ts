function prinStatusCode(code: string|number){
    if(typeof code === 'string'){
        console.log(`My status code is ${code.toUpperCase()} ${typeof code}`);
    } else {
        console.log(`My status code is ${code} ${typeof code}`);
    }
}

prinStatusCode(404);
prinStatusCode("abc");